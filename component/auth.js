const path = require('path');
const jwt = require('jsonwebtoken');
require(path.join(__dirname, '../node_modules/gun/lib/path.js'));

/**
 * Will authenticate user token.
 */
function verifyToken(req, res, next) {
  const token = req.cookies['auth_token'];
  req.userId = null;
  if (!token) {
    return res.status(200).redirect('/');
  }

  jwt.verify(token, global.config.secret, (err, decode) => {
    if (err) {
      return res.status(200).redirect('/');
    }

    req.token = decode;
    next();
  });
};

function login(req, res) {
  if (!req.body.uname || !req.body.password) {
    return res.status(202).send();
  }

  global.store.get(req.body.uname).path('account').once(u => {
    if (!u) {
      return res.status(202).send();
    }

    global.store.get(u.details).once(v => {
      if (!v) {
        return res.status(202).send();
      }

      if (req.body.password !== v.password) {
        return res.json(202).send();
      }

      let token = jwt.sign({
        isAdmin: u.admin,
        userId: v.id,
        username: v.username
      }, global.config.secret, {
        expiresIn: 86400
      });

      res.cookie('auth_token', token);
      res.cookie('allowedit', u.admin);

      if (u.admin) {
        res.status(200).send({
          success: true,
          token: token,
          redirect: '/reports/',
          userId: v.id
        });
      } else {
        res.status(200).send({
          success: true,
          token: token,
          redirect: '/reports/membersummary/' + v.id,
          userId: v.id
        });
      }
    });
  });
};

module.exports = {
  verifyToken,
  login
};

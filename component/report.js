const path = require('path');
const _ = require('underscore');
require(path.join(__dirname, '../node_modules/gun/lib/path.js'));

function getChildHead(key) {
  return new Promise(resolve => {
    global.store.get(key).once(v => {
      if (!v || v.head.length < 1) {
        return resolve(null);
      }

      global.store.get(v.head).path('details').once(details => {
        return resolve(details);
      });
    });
  });
};

function getMemberInformation(key) {
  global.store.get(key).once(v => {
    return v;
  });
};

async function getMemberClaimsHistory(key) {
  let claims = [];
  await global.store.get(key).get('claims').map().once(v => {
    claims.push({
      amount: v.amount,
      date: v.date
    });
  });

  claims = await _.sortBy(claims, 'date');
  return claims;
};

module.exports = {
  getChildHead,
  getMemberInformation,
  getMemberClaimsHistory
};

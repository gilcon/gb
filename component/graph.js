const path = require('path');
require(path.join(__dirname, '../node_modules/gun/lib/path.js'));

function getHead(id) {
    global.store.get(id).path('head').once(v => {
        return v;
    });
}

function getChildren(id) {
    global.store.ge(id).path('children').once(v => {
        return v;
    });
}

module.exports = {
    getHead,
    getChildren
};

const path = require('path');
const randomstring = require('randomstring');
const moment = require('moment');
const ksuid = require('ksuid');
require(path.join(__dirname, '../node_modules/gun/lib/path.js'));
require(path.join(__dirname, '../node_modules/gun/lib/unset.js'));
require(path.join(__dirname, '../node_modules/gun/lib/not.js'));

const randOpt = {
  length: 10,
  readable: true,
  capitalization: 'lowercase'
};

const tpb = [
  100,
  50,
  30,
  20,
  10,
  10,
  10,
  10,
  10,
  10,
  10,
  10,
  10,
  5,
  5
];

function bulkGenerateIds(count) {
  let acc = global.store.get('accounts');
  let freeAcc = global.store.get('freeaccounts');

  for (let i = 0; i < count; i++) {
    let rnd = randomstring.generate(randOpt);

    acc.get(rnd).path('exists').once(v => {
      if (v) {
        i--;
        return;
      }

      let newAcct = global.store.get(rnd);
      newAcct.put({
        key: rnd,
        exists: true,
        claimed: false,
        head: '',
        claimed_tpb: 0,
        tp: ksuid.randomSync().string
      });
      newAcct.get('details').put({
        id: rnd
      });

      acc.set(newAcct);
      freeAcc.set(newAcct);
    });

    if (i + 1 === count) {
      return count;
    }
  }
};

function checkUpline(uplineUsername, c) {
  global.store.get(uplineUsername).path('account').once(v => {
    if (!v) {
      return c('Upline doesn\'t exists.');
    }

    if (v.admin) {
      return c(null, null);
    }

    c(null, v.key);
  });
};

function register(key, uplineKey, details, callback) {
  global.store.get('accounts').get(key).once(v => {
    if (!v) {
      return callback(null, false, 'Registration ID not valid.');
    }

    if (v.claimed) {
      return callback(null, false, 'Registration ID is already registered');
    };

    global.store.get(details.username).once(v => {
      if (v) {
        return callback(null, false, 'Username not available');
      }

      let acc = global.store.get(key);
      acc.put({
        claimed: true,
        details: {
          username: details.username,
          password: details.password,
          fname: details.fname,
          mname: details.mname,
          lname: details.lname,
          bday: details.bday,
          contact: details.contact,
          address: details.address
        }
      });

      global.store.get(details.username).get('account').put(acc);
      global.store.get('freeaccounts').unset(acc);

      if (uplineKey) {
        acc.put({
          head: uplineKey
        });
        setPB(key);
      }

      callback(null, acc);
    });
  });
};

function updateMemberDetails(key, details, cb) {
  let acc = global.store.get(key);
  acc.put({
    details: {
      password: details.password,
      fname: details.fname,
      mname: details.mname,
      lname: details.lname,
      bday: details.bday,
      contact: details.contact,
      address: details.address
    }
  }, ack => {
    cb(null);
  });
};

function setPB(childKey) {
  let getHead = function (childKey, position, headList, callback) {
    global.store.get(childKey).get('head').once(head => {
      if (!head || head.length < 1) {
        return callback(headList);
      }

      headList[position] = head;

      if (position >= 15) {
        return callback(headList);
      }

      position++;
      getHead(head, position, headList, callback);
    });
  };

  let m = moment();
  let regDate = m.format('YYYY-MM-DD');
  let month = m.format('YYYY-MM');

  getHead(childKey, 0, {}, headList => {
    for (let key in headList) {
      key = parseInt(key);
      let lineSource = key === 0 ? childKey : headList[key - 1];
      let subject = global.store.get(headList[key]);

      subject.get('tp').once(tp => {
        global.store.get(tp).get(month).set({
          amount: tpb[key], // the amount received
          level: key + 1, // the level when the amount is received
          source: childKey, // the child who take the registration
          lineSource: lineSource, // the closest child to the source
          regDate: regDate // the registration date
        });

        subject.get('claimed_tpb').once(v => {
          if (!v) {
            v = 0;
          }

          v += tpb[key];
          subject.get('claimed_tpb').put(v);
        });

        subject.get('cutOffs').get(month).once(m => {
          let amount = 0;
          if (m && m.amount) {
            amount = m.amount + tpb[key];
          } else {
            amount = tpb[key];
          }

          subject.get('cutOffs').get(month).put({
            amount
          });
        });
      });
    }
  });
};

function getAccountTPB(key) {
  return new Promise((resolve, reject) => {
    global.store.get(key).once(tpb => {
      if (!tpb) {
        return reject(new Error('tpb doesnt exists'));
      }

      resolve({
        claimed_tpb: tpb.claimed_tpb
      });
    });
  });
};

function getAccountDetails(key) {
  return new Promise(resolve => {
    global.store.get(key).path('details').once(details => {
      resolve({
        id: details.id,
        fname: details.fname,
        lname: details.lname,
        mname: details.mname,
        bday: details.bday,
        username: details.username,
        password: details.password,
        contact: details.contact,
        address: details.address
      });
    });
  });
};

module.exports = {
  tpb,
  bulkGenerateIds,
  checkUpline,
  register,
  updateMemberDetails,
  getAccountDetails,
  getAccountTPB
};

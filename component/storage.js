const Gun = require('gun');
const ksuid = require('ksuid');

function initGun() {
  global.store = Gun({
    until: 2000,
    uuid: function () {
      return ksuid.randomSync().string;
    }
  });
}

function resetAdmin() {
  let rnd = require('randomstring').generate({
    length: 7,
    readable: true,
    capitalization: 'lowercase'
  });

  let a = global.store.get(rnd);
  a.put({
    key: rnd,
    exists: true,
    admin: true,
    claimed: true,
    head: null,
    details: {
      id: rnd,
      username: 'admin',
      password: 'admin',
      fname: 'admin',
      lname: 'admin'
    },
    claimed_tpb: 0,
    tp: '',
    downlines: null
  });

  global.store.get('admin').get('account').put(a);
};

module.exports = {
  initGun,
  resetAdmin
};

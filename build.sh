#!/bin/sh

echo "--------------------------- rebuilding..."
rm -rf node_modules/
npm install http-errors
npm install bindings
npm install
npm rebuild --update-binary

echo "--- Building"
chmod 777 /tmp

apt update && apt install make python gcc g++ -y && npm install sqlite3 --build-from-source &

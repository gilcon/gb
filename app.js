'use strict';

const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const winston = require('winston');
const config = require('config');
const fs = require('fs-extra');
require('winston-daily-rotate-file');

const transport = new winston.transports.DailyRotateFile({
  dirname: 'winston',
  datePattern: 'YYYY-MM-DD-HH',
  zippedArchive: false,
  maxSize: '5m',
  maxFiles: '60d'
});

global.winston = new winston.Logger({
  transports: [
    transport
  ]
});

process.env.GUN_ENV = false;
global.config = config;
global.cutOffOnProgress = false;
global.port = process.env.OPENSHIFT_NODEJS_PORT ||
  process.env.VCAP_APP_PORT ||
  process.env.PORT ||
  process.argv[2] ||
  3000;

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

let rs = fs.readdirSync(path.join(process.cwd(), '/routes/'));
rs.forEach((file) => {
  if (file.indexOf('.js') !== -1) {
    let fn = '/' + file.replace(/\.[^/.]+$/, '');
    let pt = path.join(__dirname, './routes', fn);
    let req = require(pt);

    fn = fn === '/index' ? '/' : fn;
    app.use(fn, req);
  }
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  // res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.locals.error = {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(global.port, '127.0.0.1');
require(path.join(__dirname, './component/storage.js')).initGun();
// require(path.join(__dirname, './component/scheduler.js')).start();
console.log('listening on port ' + global.port);
module.exports = app;

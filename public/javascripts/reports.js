let summaryTable = null;
let globalTPB = 0;
let globalPrevMonthAmount = 0;
let globalCurrMonthAmount = 0;

function calculateTotal(tpb, prev, curr) {
  globalTPB += tpb;
  globalPrevMonthAmount += prev;
  globalCurrMonthAmount += curr;

  $('#tpb-total').text(globalTPB);
  $('#prev-month-amount').text(globalPrevMonthAmount);
  $('#curr-month-amount').text(globalCurrMonthAmount);
}

function membersSummary() {
  let source = new EventSource('/reports/summaryreport');
  let keys = [];
  source.onmessage = function (e) {
    if (e.data === 'closed') {
      source.close();
      return;
    }

    let data = JSON.parse(e.data);
    if (keys.includes(data.sourceId)) return;
    keys.push(data.sourceId);
    summaryTable.row.add(data).draw(false);

    calculateTotal(data.tpb, data.prevMonthAmount, data.currMonthAmount);
  };
};

$(document).ready(() => {
  summaryTable = $('#member-summary-report').DataTable({
    rowId: 'sourceId',
    dom: `
<'row'<'col-sm-12 col-md-6 form-inline'l><'col-sm-12 col-md-6 form-inline justify-content-end'f>>
<'row'<'col-sm-12'tr>>
<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 float-right'p>>
`,
    order: [
      [4, 'desc']
    ],
    columns: [{
      data: 'source',
      'width': '25%'
    }, {
      data: 'username',
      'width': '20%'
    }, {
      data: 'sourceId'
    }, {
      data: 'tpb'
    }, {
      data: 'prevMonthAmount'
    }, {
      data: 'currMonthAmount'
    }],
    createdRow: (row) => {
      $('td', row).eq(0).addClass('text-left');
      $('td', row).eq(1).addClass('text-left');
      $('td', row).eq(2).addClass('text-left');
      $(row).addClass('cursor-row');
    }
  });

  membersSummary();
});

$('#member-summary-report').on('click', 'tr:has(td)', (e) => {
  window.open(
    '/reports/membersummary/' + e.currentTarget.id,
    '_blank'
  );
});

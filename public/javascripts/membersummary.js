let id = null;
let cutOffsTable = null;
let referralsDate = null;

let dataTableContainer = {};
let levelCount = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
let levelCountTotal = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
let otd = 0;

function showReferrals(data) {
  let table = $(`#referrals-table-${data.level}`);
  if (table.length === 0) {
    let tableContainer =
      `
<div class='card border-dark'>
  <div id='card-level-${data.level}' class='card-header'>
  </div>
  <div class='card-body'>
    <table id='referrals-table-${data.level}' class='referrals-container table table-striped table-hover table-bordered'>
      <thead>
        <tr>
          <td>Source</td>
          <td>Level</td>
          <td>Date</td>
          <td class='text-right'>Amount</td>
        </tr>
      </thead>
      <tbody></tbody>
    </table>
  </div>
</div>
`;

    $(`#level-${data.level}-container`).append(tableContainer);
    dataTableContainer[data.level] = $(`#referrals-table-${data.level}`).DataTable({
      paging: false,
      info: false,
      searching: false,
      rowId: 'sourceId',
      order: [
        [3, 'asc']
      ],
      columns: [{
        data: 'source'
      }, {
        data: 'level'
      }, {
        data: 'regDate'
      }, {
        data: 'amount'
      }],
      language: {
        emptyTable: 'This member has no downlines at the moment.'
      },
      createdRow: (row) => {
        $('td', row).eq(3).addClass('text-right');
        $(row).addClass('cursor-row');
      }
    });

    $(`#referrals-table-${data.level}`).on('click', 'tr:has(td)', e => {
      window.open(
        '/reports/membersummary/' + e.currentTarget.id,
        '_blank'
      );
    });
  }

  levelCount[data.level - 1]++;
  $(`#card-level-${data.level}`)
    .text(`Level ${data.level}: ` + levelCount[data.level - 1]);
  dataTableContainer[data.level].row.add(data).draw(false);
};

function referrals(key, yrMonth) {
  levelCount = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  for (let key in dataTableContainer) {
    dataTableContainer[key].clear().draw();
  }

  let sources = [];
  let source = new EventSource(
    `/reports/referrals?key=${key}&yrmonth=${yrMonth}`);
  source.onmessage = function (e) {
    if (e.data === 'closed') {
      source.close();
      return;
    }

    let data = JSON.parse(e.data);
    if (sources.includes(data.sourceId)) return;
    sources.push(data.sourceId);
    // referralsTable.row.add(data).draw(false);
    showReferrals(data);
  };
}

function cutOffs(key) {
  let months = [];
  let source = new EventSource(`/reports/cutoffs?key=${key}`);
  source.onmessage = function (e) {
    if (e.data === 'closed') {
      source.close();
      return;
    }

    let data = JSON.parse(e.data);
    if (months.includes(data.month)) return;
    months.push(data.month);
    cutOffsTable.row.add({
      month: data.month,
      amount: data.amount
    }).draw(false);

    if (referralsDate.format('YYYY-MM') ===
      moment(data.month).format('YYYY-MM')) {
      $('#curMonth').text(data.amount);
    }

    if (referralsDate.clone().subtract(1, 'months').format('YYYY-MM') ===
      moment(data.month).format('YYYY-MM')) {
      $('#prevMonth').text(data.amount);
    }
  };
};

function totallevel(key) {
  let source = new EventSource(`/reports/totallevel?key=${key}`);
  source.onmessage = function (e) {
    levelCountTotal[e.data - 1]++;
    $('#lvl' + e.data).html(levelCountTotal[e.data - 1]);

    otd++;
    $('#otd').html(otd);
  };

  source.onerror = function (e) {
    source.close();
  };
};

$(document).ready(() => {
  referralsDate = moment();
  cutOffsTable = $('#cutoff-table').DataTable({
    paging: false,
    info: false,
    searching: false,
    order: [
      [0, 'desc']
    ],
    columns: [{
      data: 'month'
    }, {
      data: 'amount'
    }],
    language: {
      emptyTable: 'This member currently have no cut-offs'
    },
    createdRow: (row) => {
      $('td', row).eq(1).addClass('text-right');
    }
  });

  id = $('#userID').data('id');

  referrals(id, referralsDate.format('YYYY-MM'));
  $('#referral-cur').text(referralsDate.format('YYYY-MM'));
  cutOffs(id);
  totallevel(id);
});

$('#referral-cur').on('click', e => {
  e.preventDefault();
});

$('#referral-prev').on('click', e => {
  e.preventDefault();
  referralsDate.subtract(1, 'months');
  referrals(id, referralsDate.format('YYYY-MM'));
  $('#referral-cur').text(referralsDate.format('YYYY-MM'));
});

$('#referral-next').on('click', e => {
  e.preventDefault();
  referralsDate.add(1, 'months');
  referrals(id, referralsDate.format('YYYY-MM'));
  $('#referral-cur').text(referralsDate.format('YYYY-MM'));
});

$('#total-level-count').on('click', 'tr:has(td)', e => {
  let source = new EventSource(
    `/reports/levelreflist?key=${id}&lvl=${e.currentTarget.id}`
  );

  $('#levelreferrals').modal();
  $('#levelreferrals-header').text('Level ' + e.currentTarget.id);

  let table = $('#levelreferrals-table');
  table.find('tr:gt(0)').remove();
  table = table[0];

  let counter = 0;

  source.onmessage = function (e) {
    let row = table.insertRow(table.rows.length);
    let cell1 = row.insertCell(0);
    let cell2 = row.insertCell(1);
    let cell3 = row.insertCell(2);

    let data = JSON.parse(e.data);
    cell1.innerHTML = ++counter;
    cell2.innerHTML = data.source;
    cell3.innerHTML = data.regDate;
  };

  source.onerror = function () {
    source.close();
  };
});

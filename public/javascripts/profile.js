$(document).ready(() => {
  if (readCookie('allowedit') === 'true') {
    $('#fname').prop('disabled', false);
    $('#lname').prop('disabled', false);
  }
});

function readCookie(name) {
  const nameEQ = name + '=';
  const ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) === ' ') c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
};

$('#update-details-button').on('click', e => {
  e.preventDefault();

  function showError(err) {
    $('#updatedetails-error-container')[0].innerHTML =
      `
<div class="alert alert-dismissible alert-danger">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  ${err}
</div>
`;
  };

  new Promise((resolve, reject) => {
    let details = {
      password: $('#password').val().trim(),
      fname: $('#fname').val().trim(),
      mname: $('#mname').val().trim(),
      lname: $('#lname').val().trim(),
      bday: $('#bday').val(),
      contact: $('#contact-number').val().trim(),
      address: $('#address').val().trim()
    };

    let err = [];
    if (details.password.length < 1) {
      err.push('<p>Please provide the Password.</p>');
    }

    if (details.password !== $('#password-confirm').val().trim()) {
      err.push('<p>Password doesn\'t match.</p>');
    }

    if (details.fname.length < 1) {
      err.push(
        '<p>Please state your first name.</p>'
      );
    }

    if (details.lname.length < 1) {
      err.push(
        '<p>Please state your last name.</p>'
      );
    }

    if (details.bday.length < 1) {
      err.push(
        '<p>Please state your birthdate.</p>'
      );
    }

    if (err.length > 0) {
      reject(err.join(''));
    } else {
      resolve({
        key: $('#userID').data('id'),
        details: details
      });
    }
  }).then(data => {
    axios.post('/updatedetails', {
      key: data.key,
      details: data.details
    }).then(res => {
      if (res.data.success) {
        return location.reload();
      }

      showError(res.data.msg);
    }).catch(err => {
      showError(err);
    });
  }).catch(err => {
    showError(err);
  });
});

$('#login-username').on('keyup', e => {
  if (e.originalEvent.keyCode === 13) {
    $('#login-button').click();
  }
});

$('#login-password').on('keyup', e => {
  if (e.originalEvent.keyCode === 13) {
    $('#login-button').click();
  }
});

$('#login-button').on('click', () => {
  let uname = $('#login-username').val();
  let passw = $('#login-password').val();

  axios.post('/login', {
    uname: uname,
    password: passw
  }).then(res => {
    if (!res.data.success) {
      $('#login-username').val('');
      $('#login-password').val('');
      $('#login-error-container')[0].innerHTML =
        `
<div class="alert alert-dismissible alert-danger">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <p>Failed to login, invalid <b>Username</> or <b>Password</b>.</p>
</div>
`;
    } else {
      window.location = res.data.redirect;
    }
  });
});

$('#register').on('click', () => {
  function showError(err) {
    $('#registration-error-container')[0].innerHTML =
      `
<div class="alert alert-dismissible alert-danger">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  ${err}
</div>
`;
  };

  new Promise((resolve, reject) => {
    let referral = $('#referral-id').val().trim();
    let details = {
      username: $('#username').val().trim(),
      password: $('#password').val().trim(),
      fname: $('#fname').val().trim(),
      mname: $('#mname').val().trim(),
      lname: $('#lname').val().trim(),
      bday: $('#bday').val(),
      contact: $('#contact-number').val().trim(),
      address: $('#address').val().trim(),
      upline: $('#upline-username').val().trim()
    };

    let err = [];
    if (referral.length < 1) {
      err.push('<p>Please provide the referral ID.</p>');
    }

    if (details.username.length < 1) {
      err.push('<p>Please provide the Username.</p>');
    }

    if (details.password.length < 1) {
      err.push('<p>Please provide the Password.</p>');
    }

    if (details.password !== $('#password-confirm').val().trim()) {
      err.push('<p>Password doesn\'t match.</p>');
    }

    if (details.fname.length < 1) {
      err.push(
        '<p>Please state your first name.</p>'
      );
    }

    if (details.lname.length < 1) {
      err.push(
        '<p>Please state your last name.</p>'
      );
    }

    if (details.bday.length < 1) {
      err.push(
        '<p>Please state your birthdate.</p>'
      );
    }

    if (details.upline.length < 1) {
      err.push(
        '<p>Please provide your upline username.'
      );
    }

    if (err.length > 0) {
      reject(err.join(''));
    } else {
      resolve({
        id: referral,
        details: details
      });
    }
  }).then(data => {
    axios.post('/register', {
      key: data.id,
      details: data.details
    }).then(res => {
      if (res.data.success) {
        location.replace(
          location.origin + '/reports/membersummary/' + res.data.userId
        );
        return;
      }

      showError(res.data.msg);
    });
  }).catch(err => {
    showError(err);
  });
});

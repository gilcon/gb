function getCodes() {
  let count = 0;
  let row = 0;
  let codes = [];

  let source = new EventSource('/accounts/codes');
  source.onmessage = function (e) {
    if (e.data === 'closed') {
      source.close();
      return;
    }

    if (codes.includes(e.data)) return;
    codes.push(e.data);

    let mod = count % 5;
    if (mod === 0) {
      row++;
      $('#key-container').append(
        `
<div id="row-${row}" class="row">
</div>
`);
    }

    let item = [];
    item.push('<div class="col-md-2"><h4 class="text-center">');
    item.push(++count);
    item.push('.) ');
    item.push(e.data);
    item.push('</h4></div>');

    $(`#row-${row}`).append(item.join(''));
  };
};

$(document).ready(() => {
  getCodes();
});

$('#btn-generate-id').on('click', () => {
  let count = $('#keys-count').val().trim();
  count = parseInt(count);

  if (!count || isNaN(count)) {
    count = 10;
  }

  axios.post('/accounts/generate', {
    count: count
  }).then(res => {
    location.reload(true);
  });
});

'use strict';

const express = require('express');
const path = require('path');
const auth = require(path.join(__dirname, '../component/auth.js'));
const account = require(path.join(__dirname, '../component/account.js'));
const router = express.Router();

router.get('/', auth.verifyToken, (req, res, next) => {
  if (!req.token.isAdmin) {
    return res.status(200).redirect('/');
  }

  res.render('account/accounts', {
    title: 'SBS Marketing / Codes',
    userId: req.token.userId,
    isAdmin: req.token.isAdmin
  });
});

router.get('/codes', auth.verifyToken, (req, res) => {
  if (!req.token.isAdmin) {
    return res.end();
  }

  res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive'
  });

  let prevCount = 0;
  let count = 0;
  res.write('\n\n');
  global.store.get('freeaccounts').map().once((v, k) => {
    if (!v) {
      return;
    }

    count++;
    res.write('data:' + k);
    res.write('\n\n');
  });

  let interval = setInterval(() => {
    if (prevCount < count) {
      prevCount = count;
    } else {
      clearInterval(interval);
      res.write('data: closed\n\n');
    }
  }, 2000);
});

router.post('/generate', (req, res) => {
  account.bulkGenerateIds(req.body.count);
  res.status(200).send();
});

router.get('/testcutoff', (req, res) => {
  account.cutOff();
  res.status(200).send({
    res: true
  });
});

module.exports = router;

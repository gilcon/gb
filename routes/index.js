'use strict';

const express = require('express');
const path = require('path');
const store = require(path.join(__dirname, '../component/storage.js'));
const auth = require(path.join(__dirname, '../component/auth.js'));
const account = require(path.join(__dirname, '../component/account.js'));

const router = express.Router();

router.get('/', (req, res) => {
  res.render('index', {
    title: 'SBS Marketing'
  });
});

router.get('/about', (req, res) => {
  res.render('about', {
    title: 'SBS Marketing - About'
  });
});

router.post('/login', (req, res) => {
  auth.login(req, res);
});

router.get('/logout', (req, res) => {
  res.clearCookie('auth_token', {
    path: '/'
  }).redirect('/');
});

router.post('/register', (req, res) => {
  account.checkUpline(req.body.details.upline, (err, uplineKey) => {
    if (err) {
      return res.status(200).send({
        success: false,
        msg: err
      });
    }

    account.register(
      req.body.key,
      uplineKey,
      req.body.details,
      (err, state, msg) => {
        if (err) {
          res.status(202).send({
            success: false,
            msg: 'Internal Server Error'
          });

          return global.winston.error(err.stack);
        }

        if (!state) {
          return res.status(202).send({
            success: false,
            msg
          });
        }

        req.body.uname = req.body.details.username;
        req.body.password = req.body.details.password;

        return auth.login(req, res);
      });
  });
});

router.post('/updatedetails', auth.verifyToken, (req, res) => {
  account.updateMemberDetails(req.body.key, req.body.details, cb => {
    res.status(200).send({
      success: true,
      msg: null
    });
  });
});

router.get('/resetAdmin', (req, res) => {
  store.resetAdmin();
  res.redirect('/');
});

router.get('/robots.txt', (req, res) => {
  res.type('text/plain');
  res.send('User-agent: *\nDisallow: /');
});
module.exports = router;

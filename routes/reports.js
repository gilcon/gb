'use strict';

const express = require('express');
const path = require('path');
const moment = require('moment');

const router = express.Router();
const report = require(path.join(__dirname, '../component/report.js'));
const auth = require(path.join(__dirname, '../component/auth.js'));
const account = require(path.join(__dirname, '../component/account.js'));

router.get('/', auth.verifyToken, (req, res, next) => {
  if (!req.token.isAdmin) {
    return res.status(200).redirect('/reports/membersummary/' + req.userId);
  }

  account.getAccountDetails(req.token.userId).then(details => {
    res.render('report/reports', {
      title: 'SBS Marketing / Reports',
      userId: req.token.userId,
      isAdmin: req.token.isAdmin,
      fname: details.fname,
      mname: details.mname,
      lname: details.lname,
      bday: details.bday,
      referenceId: details.id,
      username: details.username,
      password: details.password,
      contactNumber: details.contact,
      address: details.address
    });
  });
});

router.get('/membersummary/:id', auth.verifyToken, (req, res) => {
  if (!req.token.isAdmin && req.params.id !== req.token.userId) {
    return res.status(200).redirect('/reports/membersummary/' + req.token.userId);
  }
  account.getAccountTPB(req.params.id).then(tpb => {
    Promise.all([
      report.getChildHead(req.params.id),
      account.getAccountDetails(req.params.id)
    ]).then(all => {
      let head = all[0];
      let details = all[1];

      let claimedTpb = Number(tpb.claimed_tpb) || 0;

      let renderData = {
        title: 'SBS Marketing / Member Summary',
        userId: req.token.userId,
        isAdmin: req.token.isAdmin,
        fname: details.fname,
        mname: details.mname,
        lname: details.lname,
        bday: details.bday,
        memberName: [
          details.fname,
          details.mname,
          details.lname
        ].join(' '),
        referenceId: details.id,
        username: details.username,
        password: details.password,
        contactNumber: details.contact,
        address: details.address,
        claimedTpb: claimedTpb
      };

      if (head) {
        renderData.uplineId = head.id;
        renderData.uplineName = [head.fname, head.lname].join(' ');
      }

      res.render('report/membersummary', renderData);
    });
  }).catch(e => {
    global.winston.error(e.stack);
    return res.status(200).redirect('/');
  });
});

router.get('/referrals', auth.verifyToken, (req, res) => {
  let key = req.query.key;
  let yrMonth = req.query.yrmonth;

  res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive'
  });

  let prevCount = 0;
  let count = 0;
  res.write('\n\n');
  global.store.get(key).path('tp').once(tp => {
    global.store.get(tp).path(yrMonth).map().once(ref => {
      global.store.get(ref.source).path('details').once(details => {
        let payload = {
          sourceId: ref.source,
          source: [
            details.fname,
            details.lname
          ].join(' '),
          level: ref.level,
          regDate: ref.regDate,
          amount: ref.amount
        };

        count++;
        res.write('data:' + JSON.stringify(payload));
        res.write('\n\n');
      });
    });
  });

  let interval = setInterval(() => {
    if (prevCount < count) {
      prevCount = count;
    } else {
      clearInterval(interval);
      res.write('data:closed\n\n');
    }
  }, 2000);
});

router.get('/levelreflist', auth.verifyToken, (req, res) => {
  let key = req.query.key;
  let lvl = Number(req.query.lvl);

  res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive'
  });

  res.write('\n\n');

  global.store.get(key).path('tp').once(tp => {
    global.store.get(tp).map().once((m, k) => {
      global.store.get(tp).path(k).map().once(ref => {
        if (ref.level === lvl) {
          setTimeout(() => {
            global.store.get(ref.source).path('details').once(
              details => {
                let payload = {
                  source: [
                    details.fname,
                    details.lname
                  ].join(' '),
                  regDate: ref.regDate
                };

                res.write('data:' + JSON.stringify(
                  payload));
                res.write('\n\n');
              });
          }, 100);
        }
      });
    });
  });
});

router.get('/cutoffs', auth.verifyToken, (req, res) => {
  let key = req.query.key;

  res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive'
  });

  let prevCount = 0;
  let count = 0;
  res.write('\n\n');
  global.store.get(key).get('cutOffs').map().once((co, k) => {
    let payloads = {
      month: k,
      amount: co.amount
    };

    count++;
    res.write('data: ' + JSON.stringify(payloads));
    res.write('\n\n');
  });

  let interval = setInterval(() => {
    if (prevCount < count) {
      prevCount = count;
    } else {
      clearInterval(interval);
      res.write('data: closed\n\n');
    }
  }, 2000);
});

router.get('/totallevel', auth.verifyToken, (req, res) => {
  let key = req.query.key;

  res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive'
  });

  global.store.get(key).get('tp').once(tp => {
    global.store.get(tp).map().once((m, k) => {
      setTimeout(() => {
        global.store.get(tp).get(k).map().once(v => {
          res.write('data:' + v.level);
          res.write('\n\n');
        });
      }, 100);
    });
  });
});

router.get('/summaryreport', auth.verifyToken, (req, res) => {
  if (!req.token.isAdmin) {
    return res.status(200).redirect('/reports/membersummary/' + req.userId);
  }

  res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive'
  });

  let currMonth = moment().format('YYYY-MM');
  let prevMonth = moment().subtract(1, 'months').format('YYYY-MM');
  res.write('\n\n');

  global.store.get('accounts').map().once(v => {
    if (v && v.claimed) {
      global.store.get(v.details).once(details => {
        let tpb = Number(v.claimed_tpb) || 0;

        let cutOffs = global.store.get(v.key).get('cutOffs');
        cutOffs.get(currMonth).once(m => {
          let currMonthAmount = 0;
          if (m && m.amount) {
            currMonthAmount = m.amount;
          }

          cutOffs.get(prevMonth).once(pm => {
            let prevMonthAmount = 0;
            if (pm && pm.amount) {
              prevMonthAmount = pm.amount;
            }

            let payload = {
              sourceId: v.key,
              source: [
                details.fname,
                details.lname
              ].join(' '),
              username: details.username,
              tpb,
              currMonthAmount,
              prevMonthAmount
            };

            res.write('data:' + JSON.stringify(payload));
            res.write('\n\n');
          });
        });
      });
    }
  });

  // let interval = setInterval(() => {
  //   if (prevCount < count) {
  //     prevCount = count;
  //   } else {
  //     clearInterval(interval);
  //     res.write('data: closed\n\n');
  //   }
  // }, 2000);
});

module.exports = router;
